export interface MessageModel {
  id?: string;
  fromUID: string;
  toUID: string;
  message: string;
  status: 'Read'| 'Unread';
}
