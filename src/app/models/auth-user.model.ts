export interface AuthUserModel {
  id?: string;
  uid: string;
  email: string;
  displayName?: string;
  phone?: string;
  intro?: string;
  status?: string;
}
