export interface PeopleModel {
  id?: string;
  uid: string;
  email: string;
  displayName: string;
  phone: string;
  status: 'Active' | 'Offline';
  intro?: string;
}
