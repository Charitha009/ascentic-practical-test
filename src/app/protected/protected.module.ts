import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProtectedRoutingModule} from './protected-routing.module';
import {SharedModule} from '../shared/shared.module';

import {LayoutComponent} from './layout/layout.component';
import {MainNavigationComponent} from './layout/main-navigation/main-navigation.component';
import {SidenavListComponent} from './layout/sidenav-list/sidenav-list.component';
import {SidenavProfileComponent} from './layout/sidenav-profile/sidenav-profile.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {NewUserWelcomeComponent} from './dialogs/new-user-welcome/new-user-welcome.component';
import {UserProfileComponent} from './pages/user-profile/user-profile.component';
import {PeopleComponent} from './pages/people/people.component';
import {ChatComponent} from './pages/people/chat/chat.component';
import {PeopleListComponent} from './pages/people/people-list/people-list.component';
import {InboxComponent} from './pages/people/chat/inbox/inbox.component';
import {OutboxComponent} from './pages/people/chat/outbox/outbox.component';

@NgModule({
  declarations: [
    LayoutComponent,
    MainNavigationComponent,
    SidenavListComponent,
    SidenavProfileComponent,
    DashboardComponent,
    NewUserWelcomeComponent,
    UserProfileComponent,
    PeopleComponent,
    ChatComponent,
    PeopleListComponent,
    InboxComponent,
    OutboxComponent
  ],
  imports: [
    CommonModule,
    ProtectedRoutingModule,
    SharedModule
  ]
})
export class ProtectedModule {
}
