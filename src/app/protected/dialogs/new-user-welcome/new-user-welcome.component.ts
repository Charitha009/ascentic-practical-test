import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import * as uiActions from '../../../store/ui/ui.actions';
import * as fromApp from '../../../store/app.reducer';

@Component({
  selector: 'app-new-user-welcome',
  templateUrl: './new-user-welcome.component.html',
  styleUrls: ['./new-user-welcome.component.scss']
})
export class NewUserWelcomeComponent implements OnInit {
  profileForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public passedData: {uid: string, email: string},
    private store: Store<fromApp.AppState>,
    private dialogRef: MatDialogRef<NewUserWelcomeComponent>,
  ) { }

  ngOnInit(): void {
    this.profileForm = new FormGroup({
      email: new FormControl(this.passedData.email, {validators: [Validators.required, Validators.email]}),
      displayName: new FormControl(null, {validators: [Validators.required]}),
      phone: new FormControl(null, {validators: [Validators.required, Validators.pattern(/^\+\d{8,11}(\+\d{8,11})*$/)]}),
      intro: new FormControl(null),
    });
  }

  onSubmit(): void {
    if (this.profileForm.valid) {
      this.dialogRef.close(this.profileForm.value);
    } else {
      this.store.dispatch(new uiActions.ShowSnackBarMessage('Please fill the form correctly'));
    }
  }
}
