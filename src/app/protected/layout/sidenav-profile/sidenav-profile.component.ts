import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';

import * as fromApp from '../../../store/app.reducer';
import * as authActions from '../../../store/auth/auth.actions';
import {AuthUserModel} from '../../../models/auth-user.model';

@Component({
  selector: 'app-sidenav-profile',
  templateUrl: './sidenav-profile.component.html',
  styleUrls: ['./sidenav-profile.component.scss']
})
export class SidenavProfileComponent implements OnInit {
  authUser$: Observable<AuthUserModel>;
  authSubs: Subscription;
  userDocId$ = new BehaviorSubject(null);

  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    this.authUser$ = this.store.select('auth', 'authUser');

    this.authSubs =  this.store.select('auth', 'authUser').subscribe(authUser => {
      if (authUser !== null) {
        this.userDocId$.next(authUser.id);
      }
    });
  }

  onLogout(): void {
    const docId = this.userDocId$.getValue();
    this.store.dispatch(new authActions.ChangeStatus({docId, status: 'Offline'}));
    this.store.dispatch(new authActions.LogOut());
  }
}
