import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';

import * as fromApp from '../../../store/app.reducer';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit, OnDestroy {
  isHandsetSubs: Subscription;
  isAuthenticated$: Observable<boolean>;
  isAuthLoading$: Observable<boolean>;
  @Output() navListClicked = new EventEmitter();
  isEnablingMultiNavPanelsExpanding = false;
  currentItem: string;

  constructor(
    private store: Store<fromApp.AppState>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isAuthenticated$ = this.store.select('auth', 'isAuthenticated');
    this.isAuthLoading$ = this.store.select('auth', 'isLoading');

    if (this.router.url !== '/messenger') {
      this.currentItem = this.router.url.split('/messenger/')[1].split('/')[0];
    }
  }

  onClick(): void {
    this.isHandsetSubs = this.store.select('ui', 'isHandset').subscribe(isHandset => {
      if (isHandset) {
        this.navListClicked.emit();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.isHandsetSubs) {
      this.isHandsetSubs.unsubscribe();
    }
  }

}
