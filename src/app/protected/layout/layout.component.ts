import {Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Subscription} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {Store} from '@ngrx/store';
import {AngularFirestore} from '@angular/fire/firestore';
import {MatDialog} from '@angular/material/dialog';

import * as fromApp from '../../store/app.reducer';
import * as authActions from '../../store/auth/auth.actions';
import {NewUserWelcomeComponent} from '../dialogs/new-user-welcome/new-user-welcome.component';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {
  afAuthSubs: Subscription;
  authUserSubs: Subscription;
  userData$ = new BehaviorSubject(null);

  constructor(
    private afAuth: AngularFireAuth,
    private store: Store<fromApp.AppState>,
    private afStore: AngularFirestore,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {

    this.afAuthSubs = this.afAuth.user.subscribe(user => {
      if (user !== null) {
        this.userData$.next({email: user.email, uid: user.uid});
        this.store.dispatch(new authActions.SetAuthUser({
          uid: user.uid,
          email: user.email,
          displayName: null
        }));
        this.store.dispatch(new authActions.GetProfile(user.uid));
      }
    });

    this.authUserSubs = this.store.select('auth').subscribe(auth => {
      if (!auth.isProfileUpdated) {
        const userData = this.userData$.getValue();
        this.onNewUserLogin(userData.uid, userData.email);
      }
      if (auth.authUser !== null && auth.authUser.id !== undefined) {
        this.store.dispatch(new authActions.ChangeStatus({docId: auth.authUser.id, status: 'Active'}));
      }
    });
  }

  onNewUserLogin(uid: string, email: string): void {
    const dialogRef = this.dialog.open(NewUserWelcomeComponent, {
      disableClose: true,
      data: {
        uid,
        email
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(new authActions.UpdateProfile({
          uid,
          email,
          displayName: result.displayName,
          phone: result.phone,
          intro: result.intro,
        }));
      }
    });
  }

  ngOnDestroy(): void {
    if (this.afAuthSubs) {
      this.afAuthSubs.unsubscribe();
    }
  }
}
