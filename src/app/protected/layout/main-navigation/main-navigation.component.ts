import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {Store} from '@ngrx/store';

import * as fromApp from '../../../store/app.reducer';
import * as authActions from '../../../store/auth/auth.actions';
import {AuthUserModel} from '../../../models/auth-user.model';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss']
})
export class MainNavigationComponent implements OnInit {
  @ViewChild('drawer', {static: false}) drawer: MatSidenav;
  title$: Observable<string>;
  isHandset$: Observable<boolean>;
  authUser$: Observable<AuthUserModel>;
  authSubs: Subscription;
  userDocId$ = new BehaviorSubject(null);


  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    this.title$ = this.store.select('ui', 'title');
    this.isHandset$ = this.store.select('ui', 'isHandset');
    this.authUser$ = this.store.select('auth', 'authUser');


    this.authSubs =  this.store.select('auth', 'authUser').subscribe(authUser => {
      if (authUser !== null) {
        this.userDocId$.next(authUser.id);
      }
    });
  }

  onCloseNav(): void {
    this.drawer.close();
  }

  onLogout(): void {
    const docId = this.userDocId$.getValue();
    this.store.dispatch(new authActions.ChangeStatus({docId, status: 'Offline'}));
    this.store.dispatch(new authActions.LogOut());
  }

}
