import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LayoutComponent} from './layout/layout.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {UserProfileComponent} from './pages/user-profile/user-profile.component';
import {PeopleComponent} from './pages/people/people.component';
import {ChatComponent} from './pages/people/chat/chat.component';
import {PeopleListComponent} from './pages/people/people-list/people-list.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'dashboard',
        redirectTo: '/messenger',
        pathMatch: 'full'
      },
      {
        path: 'user-profile',
        component: UserProfileComponent
      },
      {
        path: 'people',
        component: PeopleComponent,
        children: [
          {
            path: '',
            component: PeopleListComponent
          },
          {
            path: ':id/chat',
            component: ChatComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }
