import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';

import * as fromApp from '../../../store/app.reducer';
import * as uiActions from '../../../store/ui/ui.actions';
import {AuthUserModel} from '../../../models/auth-user.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  authSubs: Subscription;
  authUser: AuthUserModel;
  isHandset$: Observable<boolean>;
  isPageLoading$ = new BehaviorSubject(null);

  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    this.store.dispatch(new uiActions.UpdateTitle('My Profile'));
    this.isPageLoading$.next(true);
    this.isHandset$ = this.store.select('ui', 'isHandset');

    this.authSubs = this.store.select('auth', 'authUser').subscribe(authUser => {
      if (authUser !== null) {
        this.authUser = authUser;
        this.isPageLoading$.next(false);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.authSubs) {
      this.authSubs.unsubscribe();
    }
  }
}
