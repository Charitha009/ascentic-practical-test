import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';

import * as fromApp from '../../../store/app.reducer';
import * as uiActions from '../../../store/ui/ui.actions';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    this.store.dispatch(new uiActions.UpdateTitle('Dashboard'));
  }

}
