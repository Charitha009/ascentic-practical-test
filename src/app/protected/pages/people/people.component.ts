import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';

import * as fromApp from '../../../store/app.reducer';
import * as peopleActions from '../../../store/people/people.actions';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    this.store.dispatch(new peopleActions.GetAllPeople());
  }
}
