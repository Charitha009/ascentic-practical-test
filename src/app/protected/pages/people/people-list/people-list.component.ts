import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';

import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Store} from '@ngrx/store';

import {PAGINATOR_CONFIG} from '../../../../config/pagination.config';
import {tableExpandAnimation} from '../../../../shared/animations';
import * as fromApp from '../../../../store/app.reducer';
import * as uiActions from '../../../../store/ui/ui.actions';
import {PeopleModel} from '../../../../models/people.model';
import {AuthUserModel} from '../../../../models/auth-user.model';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: [
    './people-list.component.scss',
    '../../../../../assets/css/mat-expand-table.scss'
  ],
  animations: [
    tableExpandAnimation
  ]
})
export class PeopleListComponent implements OnInit, OnDestroy {
  peopleSearchForm: FormGroup;
  peopleSubs: Subscription;
  isHandset$: Observable<boolean>;
  authUser$: Observable<AuthUserModel>;
  isPageLoading$ = new BehaviorSubject(null);
  isPeopleLoading$: Observable<boolean>;

  // Data Table
  dataSource = new MatTableDataSource<PeopleModel>();
  columnsToDisplay = ['index', 'displayName', 'intro', 'status', 'functions'];
  expandedElement: PeopleModel | null;
  @ViewChild(MatPaginator, {static: false})
  set paginator(value: MatPaginator) {
    this.dataSource.paginator = value;
  }
  @ViewChild(MatSort, {static: false})
  set sort(value: MatSort) {
    this.dataSource.sort = value;
  }
  paginatorConfig = PAGINATOR_CONFIG;

  constructor(
    private store: Store<fromApp.AppState>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isPageLoading$.next(true);
    this.store.dispatch(new uiActions.UpdateTitle('People'));

    this.isHandset$ = this.store.select('ui', 'isHandset');
    this.isPeopleLoading$ = this.store.select('people', 'isLoading');
    this.authUser$ = this.store.select('auth', 'authUser');

    this.peopleSearchForm = new FormGroup({
      search: new FormControl(null)
    });

    this.peopleSubs = this.store.select('people', 'peoples').subscribe(peoples => {
      if (peoples !== null) {
        this.dataSource.data = peoples;
        this.isPageLoading$.next(false);
      }
    });
  }
  onSearch(): void {
    let searchTest = '';
    if (this.peopleSearchForm.value.search !== null) {
      searchTest += this.peopleSearchForm.value.search;
    }
    this.dataSource.filter = searchTest.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onClearSearchForm(): void {
    this.peopleSearchForm.reset();
    this.onSearch();
  }

  onSendMessage(peopleUid: string): void {
    this.router.navigate(['/messenger/people/' + peopleUid + '/chat']);
  }

  ngOnDestroy(): void {
    if (this.peopleSubs) {
      this.peopleSubs.unsubscribe();
    }
  }

}
