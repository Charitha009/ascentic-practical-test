import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {Store} from '@ngrx/store';

import * as fromApp from '../../../../../store/app.reducer';
import * as peopleActions from '../../../../../store/people/people.actions';
import {PAGINATOR_CONFIG} from '../../../../../config/pagination.config';
import {MessageModel} from '../../../../../models/message.model';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit, OnDestroy {
  peopleSubs: Subscription;
  isHandset$: Observable<boolean>;

  // Data Table
  dataSource = new MatTableDataSource<MessageModel>();
  columnsToDisplay = ['index', 'message', 'status', 'functions'];
  expandedElement: MessageModel | null;
  @ViewChild(MatPaginator, {static: false})
  set paginator(value: MatPaginator) {
    this.dataSource.paginator = value;
  }
  paginatorConfig = PAGINATOR_CONFIG;

  constructor(
    private store: Store<fromApp.AppState>,
  ) { }

  ngOnInit(): void {
    this.isHandset$ = this.store.select('ui', 'isHandset');

    this.peopleSubs = this.store.select('people', 'inboxMessages').subscribe(inboxMessages => {
      if (inboxMessages !== null) {
        this.dataSource.data = inboxMessages;
      }
    });
  }

  markAsRead(id: string): void {
    this.store.dispatch(new peopleActions.MarkAsRead(id));
  }

  ngOnDestroy(): void {
    if (this.peopleSubs) {
      this.peopleSubs.unsubscribe();
    }
  }
}
