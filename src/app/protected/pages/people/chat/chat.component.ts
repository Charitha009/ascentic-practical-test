import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {ActivatedRoute} from '@angular/router';

import * as fromApp from '../../../../store/app.reducer';
import * as uiActions from '../../../../store/ui/ui.actions';
import * as peopleActions from '../../../../store/people/people.actions';
import {AuthUserModel} from '../../../../models/auth-user.model';
import {PeopleModel} from '../../../../models/people.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  charForm: FormGroup;
  isPeopleLoading$: Observable<boolean>;
  isPageLoading$ = new BehaviorSubject(null);
  authSubs: Subscription;
  peopleSubs: Subscription;
  fromUser: AuthUserModel;
  toUser: PeopleModel;

  constructor(
    private store: Store<fromApp.AppState>,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.isPageLoading$.next(true);
    this.store.dispatch(new uiActions.UpdateTitle('Chat'));


    this.charForm = new FormGroup({
      message: new FormControl(null),
    });

    this.authSubs = this.store.select('auth', 'authUser').subscribe(authUser => {
      if (authUser !== null) {
        this.fromUser = authUser;
        this.store.dispatch(new peopleActions.GetMessageInbox(authUser.uid));
        this.store.dispatch(new peopleActions.GetMessageOutbox(authUser.uid));
      }
    });

    this.peopleSubs = this.store.select('people').subscribe(people => {
      if (people.peoples !== null) {
        this.toUser = people.peoples.find(item => item.uid === this.route.snapshot.params.id);
        this.isPageLoading$.next(false);
      }
    });
  }
  onSubmit(): void {

    if (this.charForm.value.message !== null) {
      const message = this.charForm.value.message.trim();
      if (message.length !== 0) {
        this.store.dispatch(new peopleActions.SendMessage({
          fromUID: this.fromUser.uid,
          toUID: this.toUser.uid,
          status: 'Unread',
          message
        }));
      } else {
        this.store.dispatch(new uiActions.ShowSnackBarMessage('No message to sent'));
      }
    } else {
      this.store.dispatch(new uiActions.ShowSnackBarMessage('Message is empty'));
    }
  }

  ngOnDestroy(): void {
    if (this.authSubs) {
      this.authSubs.unsubscribe();
    }
  }
}
