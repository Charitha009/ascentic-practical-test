import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {MessageModel} from '../../../../../models/message.model';
import {MatPaginator} from '@angular/material/paginator';
import {PAGINATOR_CONFIG} from '../../../../../config/pagination.config';
import {Store} from '@ngrx/store';
import * as fromApp from '../../../../../store/app.reducer';

@Component({
  selector: 'app-outbox',
  templateUrl: './outbox.component.html',
  styleUrls: ['./outbox.component.scss']
})
export class OutboxComponent implements OnInit, OnDestroy {
  peopleSubs: Subscription;
  isHandset$: Observable<boolean>;

  // Data Table
  dataSource = new MatTableDataSource<MessageModel>();
  columnsToDisplay = ['index', 'message', 'status'];
  expandedElement: MessageModel | null;
  @ViewChild(MatPaginator, {static: false})
  set paginator(value: MatPaginator) {
    this.dataSource.paginator = value;
  }
  paginatorConfig = PAGINATOR_CONFIG;

  constructor(
    private store: Store<fromApp.AppState>,
  ) { }

  ngOnInit(): void {
    this.isHandset$ = this.store.select('ui', 'isHandset');

    this.peopleSubs = this.store.select('people', 'outboxMessages').subscribe(outboxMessages => {
      if (outboxMessages !== null) {
        this.dataSource.data = outboxMessages;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.peopleSubs) {
      this.peopleSubs.unsubscribe();
    }
  }
}
