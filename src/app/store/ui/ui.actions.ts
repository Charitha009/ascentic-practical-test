import {Action} from '@ngrx/store';

export const UPDATE_BREAKPOINT = '[Ui] Update Breakpoint';

export const UPDATE_TITLE = '[Ui] Update Title';

export const SHOW_SNACK_BAR_MESSAGE = '[Ui] Show Snack Bar Message';

export class UpdateBreakpoint implements Action {
  readonly type = UPDATE_BREAKPOINT;
  constructor(
    public payload: boolean
  ) {}
}

export class UpdateTitle implements Action {
  readonly type = UPDATE_TITLE;
  constructor(
    public payload: string
  ) {}
}

export class ShowSnackBarMessage implements Action {
  readonly type = SHOW_SNACK_BAR_MESSAGE;
  constructor(
    public payload: string
  ) {}
}


export type UiActions =
  | UpdateBreakpoint
  | UpdateTitle
  | ShowSnackBarMessage;
