import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';

import * as uiActions from './ui.actions';
import {UiService} from '../../services/ui.service';


@Injectable()
export class UiEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private uiService: UiService
  ) {}

  @Effect({
    dispatch: false
  })
  showSnackBarMessage = this.actions$.pipe(
    ofType(uiActions.SHOW_SNACK_BAR_MESSAGE),
    tap((data: uiActions.ShowSnackBarMessage) => {
      this.uiService.showSnackbarMessage(data.payload);
    })
  );
}
