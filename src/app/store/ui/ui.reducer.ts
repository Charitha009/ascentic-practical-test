import * as uiActions from './ui.actions';

export interface State {
  isHandset: boolean;
  title: string;
}

const initState: State = {
  isHandset: true,
  title: 'Page Title'
};

export function uiReducer(state = initState, action: uiActions.UiActions): State {
  switch (action.type) {
    case uiActions.UPDATE_TITLE:
      return {
        ...state,
        title: action.payload
      };
    case uiActions.UPDATE_BREAKPOINT:
      return {
        ...state,
        isHandset: action.payload
      };
    default:
      return state;
  }
}
