import {UiEffects} from './ui/ui.effects';
import {AuthEffects} from './auth/auth.effects';
import {PeopleEffects} from './people/people.effects';

export const Effects = [
  UiEffects,
  AuthEffects,
  PeopleEffects
];
