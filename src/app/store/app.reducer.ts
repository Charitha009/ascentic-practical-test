import {ActionReducerMap, createFeatureSelector, createSelector, MetaReducer} from '@ngrx/store';

import {environment} from '../../environments/environment';

import * as fromUi from './ui/ui.reducer';
import * as fromAuth from './auth/auth.reducer';
import * as fromPeople from './people/people.reducer';

export interface AppState {
  ui: fromUi.State;
  auth: fromAuth.State;
  people: fromPeople.State;
}

export const reducers: ActionReducerMap<AppState> = {
  ui: fromUi.uiReducer,
  auth: fromAuth.authReducer,
  people: fromPeople.peopleReducer
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
