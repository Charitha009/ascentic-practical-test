import * as peopleActions from './people.actions';
import {PeopleModel} from '../../models/people.model';
import {MessageModel} from '../../models/message.model';

export interface State {
  isLoading: boolean;
  peoples: PeopleModel[];
  inboxMessages: MessageModel[];
  outboxMessages: MessageModel[];
}

const initState: State = {
  isLoading: true,
  peoples: null,
  inboxMessages: null,
  outboxMessages: null
};

export function peopleReducer(state = initState, action: peopleActions.PeopleActions): State {
  switch (action.type) {
    case peopleActions.GET_ALL_PEOPLE:
      return {
        ...state,
        isLoading: true
      };
    case peopleActions.GET_ALL_PEOPLE_COMPLETE:
      return {
        ...state,
        peoples: action.payload,
        isLoading: false
      };
    case peopleActions.SEND_MESSAGE:
      return {
        ...state,
        isLoading: true
      };
    case peopleActions.SEND_MESSAGE_COMPLETE:
      return {
        ...state,
        isLoading: false
      };
    case peopleActions.GET_MESSAGE_INBOX:
      return {
        ...state,
        isLoading: true
      };
    case peopleActions.GET_MESSAGE_INBOX_COMPLETE:
      return {
        ...state,
        inboxMessages: action.payload,
        isLoading: false
      };
    case peopleActions.GET_MESSAGE_OUTBOX:
      return {
        ...state,
        isLoading: true
      };
    case peopleActions.GET_MESSAGE_OUTBOX_COMPLETE:
      return {
        ...state,
        outboxMessages: action.payload,
        isLoading: false
      };
    case peopleActions.MARK_AS_READ:
      return {
        ...state,
        isLoading: true
      };
    case peopleActions.MARK_AS_READ_COMPLETE:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
}
