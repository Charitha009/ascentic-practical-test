import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap} from 'rxjs/operators';
import {from, of} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';

import * as peopleActions from './people.actions';
import * as uiActions from '../ui/ui.actions';
import {environment} from '../../../environments/environment';
import {PeopleModel} from '../../models/people.model';
import {MessageModel} from '../../models/message.model';

@Injectable()
export class PeopleEffects {

  constructor(
    private actions$: Actions,
    private afAuth: AngularFireAuth,
    private afStore: AngularFirestore
  ) {}

  @Effect()
  getAllPeople = this.actions$.pipe(
    ofType(peopleActions.GET_ALL_PEOPLE),
    switchMap((data: peopleActions.GetAllPeople) => {
      return from(
        this.afStore.collection('users').valueChanges()
      ).pipe(
        map((res: PeopleModel[]) => {
          return new peopleActions.GetAllPeopleComplete(res);
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('Retrieving peoples failed!!!'));
        })
      );
    })
  );

  @Effect()
  sendMessage = this.actions$.pipe(
    ofType(peopleActions.SEND_MESSAGE),
    switchMap((data: peopleActions.SendMessage) => {
      return from(
        this.afStore.collection('messages').add(data.payload)
      ).pipe(
        map(res => {
          return new peopleActions.SendMessageComplete();
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('Message send failed!!!'));
        })
      );
    })
  );

  @Effect()
  sendMessageComplete = this.actions$.pipe(
    ofType(peopleActions.SEND_MESSAGE_COMPLETE),
    map(res => {
      return new uiActions.ShowSnackBarMessage('Message Sent');
    })
  );

  @Effect()
  getMessageInbox = this.actions$.pipe(
    ofType(peopleActions.GET_MESSAGE_INBOX),
    switchMap((data: peopleActions.GetMessageInbox) => {
      return from(
        this.afStore.collection('messages', ref => ref.where('toUID', '==', data.payload)).valueChanges({
          idField: 'id',
          toUID: 'toUID',
          fromUID: 'fromUID',
          message: 'message',
          status: 'status'
        })
      ).pipe(
        map((res: MessageModel[]) => {
          return new peopleActions.GetMessageInboxComplete(res);
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('Inbox fletch failed!!!'));
        })
      );
    })
  );

  @Effect()
  getMessageOutbox = this.actions$.pipe(
    ofType(peopleActions.GET_MESSAGE_OUTBOX),
    switchMap((data: peopleActions.GetMessageOutbox) => {
      return from(
        this.afStore.collection('messages', ref => ref.where('fromUID', '==', data.payload)).valueChanges({
          idField: 'id',
          toUID: 'toUID',
          fromUID: 'fromUID',
          message: 'message',
          status: 'status'
        })
      ).pipe(
        map((res: MessageModel[]) => {
          return new peopleActions.GetMessageOutboxComplete(res);
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('Outbox fletch failed!!!'));
        })
      );
    })
  );

  @Effect()
  markAsRead = this.actions$.pipe(
    ofType(peopleActions.MARK_AS_READ),
    switchMap((data: peopleActions.MarkAsRead) => {
      return from(
        this.afStore.collection('messages').doc(data.payload).update({status: 'Read'})
      ).pipe(
        map(res => {
          return new peopleActions.MarkAsReadComplete();
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('Mark as read failed!!!'));
        })
      );
    })
  );

}
