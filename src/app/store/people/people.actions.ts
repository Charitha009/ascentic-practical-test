import {Action} from '@ngrx/store';
import {PeopleModel} from '../../models/people.model';
import {MessageModel} from '../../models/message.model';

export const GET_ALL_PEOPLE = '[People] Get All People';
export const GET_ALL_PEOPLE_COMPLETE = '[People] Get All People Complete';

export const SEND_MESSAGE = '[People] Send Message';
export const SEND_MESSAGE_COMPLETE = '[People] Send Message Complete';

export const GET_MESSAGE_INBOX = '[People] Get Message Inbox';
export const GET_MESSAGE_INBOX_COMPLETE = '[People] Get Message Inbox Complete';

export const GET_MESSAGE_OUTBOX = '[People] Get Message Outbox';
export const GET_MESSAGE_OUTBOX_COMPLETE = '[People] Get Message Outbox Complete';

export const MARK_AS_READ = '[People] Mark As Read';
export const MARK_AS_READ_COMPLETE = '[People] Mark As Read Complete';

export class GetAllPeople implements Action {
  readonly type = GET_ALL_PEOPLE;
}

export class GetAllPeopleComplete implements Action {
  readonly type = GET_ALL_PEOPLE_COMPLETE;
  constructor(
    public payload: PeopleModel[]
  ) {}
}

export class SendMessage implements Action {
  readonly type = SEND_MESSAGE;
  constructor(
    public payload: MessageModel
  ) {}
}

export class SendMessageComplete implements Action {
  readonly type = SEND_MESSAGE_COMPLETE;
}

export class GetMessageInbox implements Action {
  readonly type = GET_MESSAGE_INBOX;
  constructor(
    public payload: string // current UID
  ) {}
}
export class GetMessageInboxComplete implements Action {
  readonly type = GET_MESSAGE_INBOX_COMPLETE;
  constructor(
    public payload: MessageModel[]
  ) {}
}

export class GetMessageOutbox implements Action {
  readonly type = GET_MESSAGE_OUTBOX;
  constructor(
    public payload: string // current UID
  ) {}
}

export class GetMessageOutboxComplete implements Action {
  readonly type = GET_MESSAGE_OUTBOX_COMPLETE;
  constructor(
    public payload: MessageModel[]
  ) {}
}

export class MarkAsRead implements Action {
  readonly type = MARK_AS_READ;
  constructor(
    public payload: string // Doc Id
  ) {}
}

export class MarkAsReadComplete implements Action {
  readonly type = MARK_AS_READ_COMPLETE;
}




export type PeopleActions =
  | GetAllPeople
  | GetAllPeopleComplete
  | SendMessage
  | SendMessageComplete
  | GetMessageInbox
  | GetMessageInboxComplete
  | GetMessageOutbox
  | GetMessageOutboxComplete
  | MarkAsRead
  | MarkAsReadComplete;
