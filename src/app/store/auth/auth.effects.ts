import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {catchError, map, switchMap} from 'rxjs/operators';
import {from, of} from 'rxjs';
import {Router} from '@angular/router';

import * as authActions from './auth.actions';
import * as uiActions from '../ui/ui.actions';
import {environment} from '../../../environments/environment';
import {AuthUserModel} from '../../models/auth-user.model';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private afAuth: AngularFireAuth,
    private afStore: AngularFirestore,
    private router: Router
  ) {
  }

  @Effect()
  signUp = this.actions$.pipe(
    ofType(authActions.SIGN_UP),
    switchMap((data: authActions.SignUp) => {
      return from(this.afAuth.createUserWithEmailAndPassword(
        data.payload.email,
        data.payload.pwd,
      )).pipe(
        map(res => {
          return new authActions.SignUpComplete();
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('User sign up failed!!!'));
        })
      );
    })
  );

  @Effect()
  signUpComplete = this.actions$.pipe(
    ofType(authActions.SIGN_UP_COMPLETE),
    map((data: authActions.SignUpComplete) => {
      this.router.navigate(['/login']);
      return new uiActions.ShowSnackBarMessage('User sign up complete. Please log in');
    })
  );

  @Effect()
  logIn = this.actions$.pipe(
    ofType(authActions.LOG_IN),
    switchMap((data: authActions.LogIn) => {
      return from(
        this.afAuth.signInWithEmailAndPassword(
          data.payload.email,
          data.payload.pwd
        )
      ).pipe(
        map(res => {
          return new authActions.LogInComplete();
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('User login failed!!!'));
        })
      );
    })
  );

  @Effect()
  updateProfile = this.actions$.pipe(
    ofType(authActions.UPDATE_PROFILE),
    switchMap((data: authActions.UpdateProfile) => {
      return from(
        this.afStore.collection('users').add(data.payload)
      ).pipe(
        map(res => {
          return new authActions.UpdateProfileComplete();
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('Profile update failed!!!'));
        })
      );
    })
  );

  @Effect()
  changeStatus = this.actions$.pipe(
    ofType(authActions.CHANGE_STATUS),
    switchMap((data: authActions.ChangeStatus) => {
      const status = {status: data.payload.status};
      return from(
        this.afStore.collection('users').doc(data.payload.docId).update(status)
      ).pipe(
        map(res => {
          return new authActions.ChangeStatusComplete();
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('Mark active failed!!!'));
        })
      );
    })
  );

  @Effect()
  getProfile = this.actions$.pipe(
    ofType(authActions.GET_PROFILE),
    switchMap((data: authActions.GetProfile) => {
      return from(
        this.afStore.collection('users', ref => ref.where('uid', '==', data.payload)).valueChanges({
          idField: 'id',
          uid: 'uid',
          displayName: 'displayName',
          email: 'email',
          phone: 'phone',
          intro: 'intro',
          status: 'status'
        })
      ).pipe(
        map((res: AuthUserModel[]) => {
          return new authActions.GetProfileComplete(res[0]);
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('Retrieving profile failed!!!'));
        })
      );
    })
  );

  @Effect()
  logout = this.actions$.pipe(
    ofType(authActions.LOG_OUT),
    switchMap((data: authActions.LogOut) => {
      return from(
        this.afAuth.signOut()
      ).pipe(
        map(res => {
          this.router.navigate(['/login']);
          return new authActions.LogOutComplete();
        }),
        catchError(err => {
          if (!environment.production) {
            console.log(err);
          }
          return of(new uiActions.ShowSnackBarMessage('User logout failed!!!'));
        })
      );
    })
  );
}
