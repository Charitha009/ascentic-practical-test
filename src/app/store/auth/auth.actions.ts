import {Action} from '@ngrx/store';
import {AuthUserDataModel} from '../../models/auth-user-data.model';
import {AuthUserModel} from '../../models/auth-user.model';

export const SIGN_UP = '[Auth] Sign Up';
export const SIGN_UP_COMPLETE = '[Auth] Sign Up Complete';

export const LOG_IN = '[Auth] Log In';
export const LOG_IN_COMPLETE = '[Auth] Log In Complete';

export const MARK_AUTHENTICATED = '[Auth] Mark Authenticated';

export const CHANGE_STATUS = '[Auth] Change Status';
export const CHANGE_STATUS_COMPLETE = '[Auth] Change Status Complete';

export const UPDATE_PROFILE = '[Auth] Update Profile';
export const UPDATE_PROFILE_COMPLETE = '[Auth] Update Profile Complete';

export const GET_PROFILE = '[Auth] Get Profile';
export const GET_PROFILE_COMPLETE = '[Auth] Get Profile Complete';

export const SET_AUTH_USER = '[Auth] Set Auth User';
export const UNSET_AUTH_USER = '[Auth] Unset Auth User';

export const LOG_OUT = '[Auth] Log Out';
export const LOG_OUT_COMPLETE = '[Auth] Log Out Complete';

export class SignUp implements Action {
  readonly type = SIGN_UP;
  constructor(
    public payload: AuthUserDataModel
  ) {}
}

export class SignUpComplete implements Action {
  readonly type = SIGN_UP_COMPLETE;
}

export class LogIn implements Action {
  readonly type = LOG_IN;
  constructor(
    public payload: AuthUserDataModel
  ) {}
}

export class LogInComplete implements Action {
  readonly type = LOG_IN_COMPLETE;
}

export class MarkAuthenticated implements Action {
  readonly type = MARK_AUTHENTICATED;
}

export class ChangeStatus implements Action {
  readonly type = CHANGE_STATUS;
  constructor(
    public payload: {docId: string, status: 'Active' | 'Offline'}
  ) {}
}
export class ChangeStatusComplete implements Action {
  readonly type = CHANGE_STATUS_COMPLETE;
}


export class UpdateProfile implements Action {
  readonly type = UPDATE_PROFILE;
  constructor(
    public payload: AuthUserModel
  ) {}
}

export class UpdateProfileComplete implements Action {
  readonly type = UPDATE_PROFILE_COMPLETE;
}

export class GetProfile implements Action {
  readonly type = GET_PROFILE;
  constructor(
    public payload: string
  ) {}
}

export class GetProfileComplete implements Action {
  readonly type = GET_PROFILE_COMPLETE;
  constructor(
    public payload: AuthUserModel
  ) {}
}

export class SetAuthUser implements Action {
  readonly type = SET_AUTH_USER;
  constructor(
    public payload: AuthUserModel
  ) {}
}

export class LogOut implements Action {
  readonly type = LOG_OUT;
}

export class LogOutComplete implements Action {
  readonly type = LOG_OUT_COMPLETE;
}


export type AuthActions =
  | SignUp
  | SignUpComplete
  | LogIn
  | LogInComplete
  | MarkAuthenticated
  | ChangeStatus
  | ChangeStatusComplete
  | UpdateProfile
  | UpdateProfileComplete
  | GetProfile
  | GetProfileComplete
  | SetAuthUser
  | LogOut
  | LogOutComplete;
