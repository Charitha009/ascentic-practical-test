import * as authActions from './auth.actions';
import {UPDATE_PROFILE} from './auth.actions';
import {AuthUserModel} from '../../models/auth-user.model';

export interface State {
  isLoading: boolean;
  isAuthenticated: boolean | null;
  authUser: AuthUserModel;
  isProfileUpdated: boolean
}

const initState: State = {
  isLoading: false,
  isAuthenticated: null,
  authUser: null,
  isProfileUpdated: true
};

export function authReducer(state = initState, action: authActions.AuthActions): State {
  switch (action.type) {
    case authActions.SIGN_UP:
      return {
        ...state,
        isLoading: true
      };
    case authActions.SIGN_UP_COMPLETE:
      return {
        ...state,
        isLoading: false
      };
    case authActions.LOG_IN:
      return {
        ...state,
        isLoading: true
      };
    case authActions.LOG_IN_COMPLETE:
      return {
        ...state,
        isLoading: false
      };
    case authActions.MARK_AUTHENTICATED:
      return {
        ...state,
        isAuthenticated: true
      };
    case authActions.UPDATE_PROFILE:
      console.log(action.payload);
      return {
        ...state,
        isLoading: true
      };
    case authActions.UPDATE_PROFILE_COMPLETE:
      return {
        ...state,
        isProfileUpdated: true,
        isLoading: false
      };
    case authActions.GET_PROFILE:
      return {
        ...state,
        isLoading: true
      };
    case authActions.GET_PROFILE_COMPLETE:
      let authUserData: AuthUserModel = state.authUser;
      let isProfileUpdatedData: boolean = state.isProfileUpdated;
      if (action.payload !== undefined) {
        authUserData = action.payload;
      } else {
        isProfileUpdatedData = false;
      }
      return {
        ...state,
        authUser: authUserData,
        isProfileUpdated: isProfileUpdatedData,
        isLoading: false
      };
    case authActions.SET_AUTH_USER:
      return {
        ...state,
        authUser: action.payload
      };
    case authActions.LOG_OUT:
      return {
        ...state,
        isAuthenticated: false,
        authUser: null,
        isLoading: true
      };
    case authActions.LOG_OUT_COMPLETE:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
}
