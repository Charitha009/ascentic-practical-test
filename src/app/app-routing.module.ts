import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AngularFireAuthGuard, redirectUnauthorizedTo} from '@angular/fire/auth-guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('src/app/public/public.module').then(m => m.PublicModule)
  },
  {
    path: 'messenger',
    loadChildren: () => import('src/app/protected/protected.module').then(m => m.ProtectedModule),
    canActivate: [AngularFireAuthGuard], // Firebase SDK not providing canLoad Gard. So they ask to use like this
    data: { authGuardPipe: redirectUnauthorizedToLogin }
  },
  {
    path: '**',
    redirectTo: '/not-found',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
