import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor(
    private snackbar: MatSnackBar
  ) { }

  showSnackbarMessage(message: string): void {
    this.snackbar.open(
      message,
      'Ok',
      {
        duration: 3000
      }
    );
  }
}
