import { Injectable } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(
  ) { }

  // Confirm Password Validation
  isSamePassword(control: FormControl, form: FormGroup): {isSamePassword: boolean} | null {
    let pwd = '';
    if (form) {
      if (form.value.password !== null ) {
        pwd = form.value.pwd;
      }
    }
    if (control.value !== pwd && control.value !== null && control.value !== '') {
      return {isSamePassword: true};
    }
    return null;
  }
}
