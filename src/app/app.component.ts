import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {BreakpointObserver} from '@angular/cdk/layout';
import {Subscription} from 'rxjs';

import * as fromApp from './store/app.reducer';
import * as uiActions from './store/ui/ui.actions';
import * as authActions from './store/auth/auth.actions';
import * as breakpoints from './config/breakpoint.config';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  breakpointSubs: Subscription;
  authStateSubs: Subscription;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<fromApp.AppState>,
    private afAuth: AngularFireAuth
  ) { }

  ngOnInit(): void {
    this.breakpointSubs = this.breakpointObserver.observe([
        ...breakpoints.breakpoints
      ]
    ).subscribe(res => {
      this.store.dispatch(new uiActions.UpdateBreakpoint(res.matches));
    });

    this.authStateSubs = this.afAuth.authState.subscribe(authState => {
      if (authState !== null) {
        this.store.dispatch(new authActions.MarkAuthenticated());
      }
    });
  }

  ngOnDestroy(): void {
    if (this.breakpointSubs) {
      this.breakpointSubs.unsubscribe();
    }
    if (this.authStateSubs) {
      this.authStateSubs.unsubscribe();
    }
  }
}
