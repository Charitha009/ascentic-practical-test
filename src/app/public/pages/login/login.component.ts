import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';

import * as uiActions from '../../../store/ui/ui.actions';
import * as authActions from '../../../store/auth/auth.actions';
import * as fromApp from '../../../store/app.reducer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isAuthLoading$: Observable<boolean>;

  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, { validators: [Validators.required, Validators.email]}),
      password: new FormControl(null, { validators: [Validators.required]})
    });
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      this.store.dispatch(new authActions.LogIn({
        email: this.loginForm.value.email,
        pwd: this.loginForm.value.password
      }));
    } else {
      this.store.dispatch(new uiActions.ShowSnackBarMessage('Please fill the form correctly'));
    }
  }

  onForgotPasswordClick(): void {

  }
}
