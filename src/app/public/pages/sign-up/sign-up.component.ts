import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {Store} from '@ngrx/store';

import {HelperService} from '../../../services/helper.service';
import * as fromApp from '../../../store/app.reducer';
import * as uiActions from '../../../store/ui/ui.actions';
import * as authActions from '../../../store/auth/auth.actions';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signupForm: FormGroup;
  agreedToTerms$ = new BehaviorSubject(null);
  isAuthLoading$: Observable<boolean>;
  isHandset$: Observable<boolean>;

  constructor(
    private store: Store<fromApp.AppState>,
    private helperService: HelperService
  ) {}

  ngOnInit(): void {
    this.isAuthLoading$ = this.store.select('auth', 'isLoading');
    this.isHandset$ = this.store.select('ui', 'isHandset');

    this.signupForm = new FormGroup({
      email: new FormControl(null, {validators: [Validators.required, Validators.email]}),
      pwd: new FormControl(null, {validators: [Validators.required, Validators.minLength(8)]}),
      confirmPwd: new FormControl(null, {validators: [Validators.required, this.isSamePassword.bind(this)]})
    });
  }

  onSubmit(): void {
    if (this.signupForm.valid && this.agreedToTerms$.getValue() === true) {
      const signUpUserData = {
        email: this.signupForm.value.email,
        pwd: this.signupForm.value.pwd,
      };
      this.store.dispatch(new authActions.SignUp(signUpUserData));
    } else {
      this.store.dispatch(new uiActions.ShowSnackBarMessage('Please fill the form correctly'));
    }
  }

  onPasswordChange(): void {
    this.signupForm.patchValue({
      confirmPwd: null
    });
  }

  isSamePassword(control: FormControl): { isSamePassword: boolean } | null {
    return this.helperService.isSamePassword(control, this.signupForm);
  }

  agreedToTermsClick(event): void {
    this.agreedToTerms$.next(event.checked);
  }
}
