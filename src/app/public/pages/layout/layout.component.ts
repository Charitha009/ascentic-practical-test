import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

import * as fromApp from '../../../store/app.reducer';
import * as uiActions from '../../../store/ui/ui.actions';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {
  authStateSubs: Subscription;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    this.authStateSubs = this.afAuth.authState.subscribe(authState => {
      if (authState !== null) {
        this.router.navigate(['/messenger']);
        this.store.dispatch(new uiActions.ShowSnackBarMessage('Welcome to My Messenger'));
      }
    });
  }

  ngOnDestroy(): void {
    if (this.authStateSubs) {
      this.authStateSubs.unsubscribe();
    }
  }
}
